package com.example.demo.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.jhlabs.image.EdgeFilter;
import com.jhlabs.image.GrayscaleFilter;
import com.jhlabs.image.InvertFilter;


@Controller
public class MainController {
	
	private static String INPUT_FOLDER = "src/main/webapp/images/input/";
	private static String OUTPUT_FOLDER = "src/main/webapp/images/output/";
	
	private static String PARAM1 = "/images/input/";
	private static String PARAM2 = "/images/output/";
	
	@GetMapping("/")
	public String showHomePage(Model model) {
		return "index";
	}
	
	@PostMapping("/upload")
	public String singleFileUpload(@RequestParam("file") MultipartFile file, Model model) {
		String fileName = file.getOriginalFilename();
		String inputFilePath =  INPUT_FOLDER + fileName;
		String outputFilePath =  OUTPUT_FOLDER + fileName;
		if (file.isEmpty()) {
			return "index";
        }	      
		
		saveMultipartFileIntoServer(file , inputFilePath);
		BufferedImage img = renderImageToCartoon(inputFilePath);
        saveBufferedImageIntoServer(img, outputFilePath);
              
		model.addAttribute("inputFilePath", PARAM1 + fileName);
		model.addAttribute("outputFilePath", PARAM2 + fileName);
		return "index";
	}
	
	
	
	public void saveMultipartFileIntoServer(MultipartFile file, String filePath) {
		byte[] bytes;
		try {
			bytes = file.getBytes();
			filePath = INPUT_FOLDER + file.getOriginalFilename();
	        Path path = Paths.get(filePath);
	        Files.write(path, bytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public BufferedImage renderImageToCartoon(String filePath) {
		BufferedImage srcImg;
		BufferedImage dstImg;
		try {
			srcImg = ImageIO.read(new File(filePath));
			
			GrayscaleFilter grayscaleFilter = new GrayscaleFilter();
			InvertFilter invertFilter = new InvertFilter();
			EdgeFilter edgeFilter = new EdgeFilter();
			
			srcImg = grayscaleFilter.filter(srcImg, srcImg);
			srcImg = edgeFilter.filter(srcImg, srcImg);
			dstImg = invertFilter.filter(srcImg, srcImg);
			
			
			
			return dstImg;
//			File outputfile = new File("E:\\img\\output18.jpg");
//			ImageIO.write(dstImg, "jpg", outputfile);
//			System.out.println("Done!!!");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public void saveBufferedImageIntoServer(BufferedImage img, String outputFilePath) {
		File outputfile = new File(outputFilePath);
		try {
			ImageIO.write(img, "jpg", outputfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Done!!!");
	}
	
	
	
}
